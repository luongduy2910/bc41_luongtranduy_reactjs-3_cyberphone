import { combineReducers } from "redux" 
import { gioHangReducer } from "./GioHangReducer"
import { detailReducer } from "./DetailReducer"

export const rootReducer = combineReducers({
    gioHangReducer , 
    detailReducer
})