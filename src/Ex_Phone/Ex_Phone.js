import React, { Component } from 'react';
import ListPhone from './ListPhone';
import PhoneCart from './PhoneCart';
import DetailPhone from './DetailPhone';

class Ex_Phone extends Component {
    render() {
        return (
            <div>
                <PhoneCart/>
                <ListPhone />
                <DetailPhone/>
            </div>
        );
    }
}

export default Ex_Phone;